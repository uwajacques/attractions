<?php

use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 100; $i++) {
        	$attraction_id = rand(1, 20);
        	$user_id = rand(1, 20);

            DB::table('reviews')->insert([
            	'attraction_id' => $attraction_id,
            	'user_id' => $user_id,
                'title' => 'Review ' .$i,
                'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.",
                'rating' => rand(1, 5)
            ]);
        }
    }
}
