 document.addEventListener('DOMContentLoaded', function() {

     // Get all "navbar-burger" elements
     var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

     // Check if there are any navbar burgers
     if ($navbarBurgers.length > 0) {
         // Add a click event on each of them
         $navbarBurgers.forEach(function($el) {
             $el.addEventListener('click', function() {

                 // Get the target from the "data-target" attribute
                 var target = $el.dataset.target;
                 var $target = document.getElementById(target);

                 // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                 $el.classList.toggle('is-active');
                 $target.classList.toggle('is-active');

             });
         });
     }
 });
 
 $(document).ready(function(){

    $('.create_review').click(function(e){

        if(!$('textarea').val() ){
            e.preventDefault();
            $( ".review_text_holder" ).append( $( "<p> Comment required!</p>" ) );
        }
    })

    $('.ratings .level-item span').click(function(e){
        e.preventDefault();
        var ratings = $(this).data('rating')
        $(".ratings input").val(ratings)
        $('.ratings .level-item span i').each(function(index) {
            $( this ).removeClass( "fa-heart" ) 
            $( this ).addClass( "fa-heart-o" )
            if(index+1 <= ratings){
               $( this ).removeClass( "fa-heart-o" ) 
                $( this ).addClass( "fa-heart" )
            }
        }); 
    })
 })