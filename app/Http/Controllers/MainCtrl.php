<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainCtrl extends Controller
{
    
    public function index(){
    	$attractions = \App\Attraction::with('reviews')->get();
    	//remove inactive reviews
    	foreach ($attractions as $key => $attraction) {
			foreach($attraction->reviews as $k => $review){
				if(!$review->active){
					$attraction->reviews->forget($k);
				}
			}
    	}
    	return view('index')->with('attractions', $attractions->toArray());
    }

    public function attraction($id){
    	$attraction = \App\Attraction::find($id);
        $reviews = $attraction->reviews;
        foreach($reviews as $review){
            $review->user = $review->user;
        }
    	return view('attraction')->with('attraction', $attraction->toArray())
    							->with('reviews', $reviews->toArray());
    }


    public function top5(){
    	$attractions = \App\Attraction::with('reviews')->get();
    	 $attractions = $attractions->toArray();
    	 $attractions = $this->calcRatingAverage($attractions);
    	$orderedAttractions =  $this->orderByAverageRating($attractions, 'averageRating');
    	$top5 =  array_slice($orderedAttractions, 0, 5, true);
    	return view('top5')->with('attractions', $top5);
    }

    public function calcRatingAverage($attractions){
		for ($i=0; $i < count($attractions); $i++) { 
			$ratings = [];
    	 	for ($x=0; $x <count($attractions[$i]['reviews']);  $x++) {
    	 		$rating = $attractions[$i]['reviews'][$x]['rating'];
    	 		array_push($ratings, $rating);
    	 	}
    	 	$averageRating = array_sum($ratings) ? array_sum($ratings) / count($attractions[$i]['reviews']) : 0;
    	  	$attractions[$i]['averageRating'] = round( $averageRating, 1, PHP_ROUND_HALF_UP); 
    	}
    	return $attractions;
    }

    public function orderByAverageRating($arr, $col, $dir = SORT_DESC) {
	    $sort_col = array();
	    foreach ($arr as $key=> $row) {
	        $sort_col[$key] = $row[$col];
	    }
	     array_multisort($sort_col, $dir, $arr);
	     return $arr;
	}

}
