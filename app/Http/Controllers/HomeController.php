<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function createReview($id){
        $review = new \App\Review;
        $review->attraction_id = (int) $id;
        $review->user_id = (int) Auth::user()->id;
        $review->title = ""; 
        $review->description = Input::get('description'); 
        $review->rating = (int) Input::get('rating'); 
        $review->save();
        return redirect()->back();
    }
}
