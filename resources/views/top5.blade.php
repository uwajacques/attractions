@extends('layouts.app')

@section('content')
<section class="section">
        <div class="container">
            @foreach(array_chunk($attractions, 3) as $chunks) 
            <div class="columns">
                @foreach($chunks as $attraction)
                <div class="column is-one-third">
                    <div class="card">
                        <div class="card-image">
                            <figure class="image is-4by3">
                                <img src="{{$attraction['image']}}" alt="Placeholder image">
                            </figure>
                        </div>
                        <header class="card-header">
                            <p class="card-header-title">
                                <a href="/attraction/{{$attraction['id']}}" >{{ $attraction['title'] }} </a>
                            </p>
                        </header>
                        <div class="card-content">
                            <div class="content">
                                {{ substr($attraction['description'], 0, 150)  }}<a href="/attraction/{{$attraction['id']}}" >...</a>
                                <br><br>
                                <a href="/attraction/{{$attraction['id']}}"> {{count($attraction["reviews"])}}  Reviews</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endforeach
        </div>
</section>
@endsection
